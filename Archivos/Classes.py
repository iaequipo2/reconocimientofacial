# class that is assigned the information of the har cascades
class Rectangle:
    def __init__(self, x, y, width, height):
        self.x = float(x)
        self.y = float(y)
        self.width = float(width)
        self.height = float(height)

    # Top left point
    def TopLeft(self):
        return [self.x, self.y]

    # Top Right point
    def TopRight(self):
        return [self.x + self.width, self.y]

    # Bottom Left point
    def BottomLeft(self):
        return [self.x, self.y + self.height]

    # Bottom Right point
    def BottomRight(self):
        return [self.x + self.width, self.y + self.height]


# Class for the face information,
# this class is asigned the rectangles information
# eacch variable must be assigned a rectangle class
class RawFace:
    def __init__(self):
        self.Face = None
        self.LeftEye = None
        self.RightEye = None
        self.Nose = None
        self.Mouth = None

    # Checks if tere are no empthy values
    def CheckValues(self):
        if self.Face is None:
            return False
        elif not isinstance(self.Face, Rectangle):
            return False
        elif self.LeftEye is None:
            return False
        elif not isinstance(self.LeftEye, Rectangle):
            return False
        elif self.RightEye is None:
            return False
        elif not isinstance(self.RightEye, Rectangle):
            return False
        elif self.Nose is None:
            return False
        elif not isinstance(self.Nose, Rectangle):
            return False
        elif self.Mouth is None:
            return False
        elif not isinstance(self.Mouth, Rectangle):
            return False
        else:
            return True


# Test de cara solo es un codigo basico de ejemplo del uso de las clases
def Test():
    face = RawFace()

    rectangle = Rectangle(0, 10, 5, 3)

    print rectangle.TopLeft()
    print rectangle.TopRight()
    print rectangle.BottomLeft()
    print rectangle.BottomRight()

    print (face.CheckValues())
    face.Face = rectangle;
    print (face.CheckValues())
    face.LeftEye = rectangle;
    print (face.CheckValues())
    face.RightEye = rectangle;
    print (face.CheckValues())
    face.Mouth = rectangle;
    print (face.CheckValues())
    face.Nose = rectangle;
    print (face.CheckValues())

