import sqlite3
from PyQt4.QtCore import *
from PyQt4.QtGui import *

class InputDialog(QWidget):

    def __init__(self, parent=None):
        super(InputDialog, self).__init__(parent)

        # Create layout
        layout = QFormLayout()

        # CURP
        # Create label
        self.curp_label = QLabel('CURP')
        # Create input
        self.curp_input = QLineEdit()
        # Add to layout
        layout.addRow(self.curp_label, self.curp_input)

        # Edad
        # Create label
        self.age_label = QLabel('Edad')
        # Create input
        self.age_input = QComboBox()
        self.age_input.addItems([str(i) for i in range(13) if i > 3])
        # Add to layout
        layout.addRow(self.age_label, self.age_input)

        # Genero
        # Create label
        self.gender_label = QLabel('Genero')
        # Create input
        self.gender_input = QComboBox()
        self.gender_input.addItems(['Hombre', 'Mujer'])

        # Add to layout
        layout.addRow(self.gender_label, self.gender_input)

        self.save_button = QPushButton('Continuar')
        # Set event
        self.save_button.clicked.connect(self.save)
        # Add to layout
        layout.addRow(self.save_button)

        self.setLayout(layout)
        self.setWindowTitle('')

    def save(self):
        if self.curp_input.text() and self.age_input.currentText() and self.gender_input.currentText():
            conn = sqlite3.connect('database.db')
            c = conn.cursor()

            # Create table
            c.execute('''CREATE TABLE IF NOT EXISTS info
            (id text, age text, gender text)''')

            # Insert a row of data
            c.execute("INSERT INTO info VALUES ('%s', '%s', '%s')" % (self.curp_input.text(), self.age_input.currentText(), self.gender_input.currentText()))

            conn.commit()
            conn.close()

            print 'CURP: %s' % self.curp_input.text()
            print 'Edad: %s' % self.age_input.currentText()
            print 'Genero: %s' % self.gender_input.currentText()

            self.hide()
            self.after_save()
        else:
            self.message_box = QMessageBox()
            self.message_box.setText('Todos los campos son necesarios.')
            self.message_box.open()

    def setAfterSave(self, after_save):
        self.after_save = after_save
