import math

# Get the magnitude angle between the face doing so sqare by square
def GetDistances(rawFace):
    if rawFace.CheckValues():
        face = []
        face += SquareToSquare(rawFace.LeftEye, rawFace.RightEye, rawFace.Face)
        face += SquareToSquare(rawFace.LeftEye, rawFace.Nose, rawFace.Face)
        face += SquareToSquare(rawFace.LeftEye, rawFace.Mouth, rawFace.Face)
        face += SquareToSquare(rawFace.RightEye, rawFace.Nose, rawFace.Face)
        face += SquareToSquare(rawFace.RightEye, rawFace.Mouth, rawFace.Face)
        face += SquareToSquare(rawFace.Nose, rawFace.Mouth, rawFace.Face)
        return face
    else:
        print "There are empthy values on face"
        return
    
# Get the magnitude angle between two squares comparing the points of each square with the 2nd square
# With both sqares inside the contanerSquare
def SquareToSquare(square1, square2, containerSquare):
    point = []
    point += PointToSquare(square1.TopLeft(), square2, containerSquare)
    point += PointToSquare(square1.TopRight(), square2, containerSquare)
    point += PointToSquare(square1.BottomLeft(), square2, containerSquare)
    point += PointToSquare(square1.BottomRight(), square2, containerSquare)
    return point
    
# get the distances between a point and a square
# inside the containerSquare
def PointToSquare(point, square, containerSquare):
    distance = []
    distance += PointToPoint(point, square.TopLeft(), containerSquare)
    distance += PointToPoint(point, square.TopRight(), containerSquare)
    distance += PointToPoint(point, square.BottomLeft(), containerSquare)
    distance += PointToPoint(point, square.BottomRight(), containerSquare)
    return distance

# Calculates the magnitude angle between two points
# inside the containerSquare
def PointToPoint(point1, point2, containerSquare):
    x = abs(point1[0] - point2[0]) / containerSquare.width
    y = abs(point1[1] - point2[1]) / containerSquare.height

    x2 = pow(x, 2)
    y2 = pow(y, 2)

    return [math.sqrt(x2 + y2), math.atan(y/x)]
