
def CheckFace(face):
    left_eye = getCoords(face.LeftEye)
    right_eye = getCoords(face.RightEye)
    mouth = getCoords(face.Mouth)
    nose = getCoords(face.Nose)

    y_range = 5
    x_rage = 5

    if left_eye[1] <= right_eye[1] + y_range and left_eye[1] >= right_eye[1] - y_range  and right_eye[0] < left_eye[0] :
        middle_y = (right_eye[0] + left_eye[0]) / 2
        if nose[1] > right_eye[1]  and nose[0] <= middle_y + x_rage and nose[0] >= middle_y - x_rage:
            if mouth[1] > nose[1] and mouth[0] <= middle_y + x_rage and mouth[0] >= middle_y - x_rage:
                return True

    return False

def getCoords(face_part):
    x = face_part.x + (face_part.width / 2)
    y = face_part.y + (face_part.height / 2)
    return (x,y)
