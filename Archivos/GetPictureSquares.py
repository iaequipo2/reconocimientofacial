import cv2
import Classes

# Load cascades
faceCascade = cv2.CascadeClassifier('haarcascades\haarcascade_frontalface_alt2.xml')
eye_cascade = cv2.CascadeClassifier('haarcascades\haarcascade_eye.xml')
leyeCascade = cv2.CascadeClassifier('haarcascades/haarcascade_lefteye_2splits.xml')
reyeCascade = cv2.CascadeClassifier('haarcascades/haarcascade_righteye_2splits.xml')
mouthCascade = cv2.CascadeClassifier('haarcascades\Mouth.xml')
noseCascade = cv2.CascadeClassifier('haarcascades\Nariz.xml')


# Class which you send an image and you get the picture boxes according to haarcascades
def GetData():
    # Start video capture
    cap = cv2.VideoCapture(0)
    face = Classes.RawFace()

    while not face.CheckValues():
        # hacer una captura
        ret, img = cap.read()
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        face = Classes.RawFace()

        if hasattr(cv2, "cv"):
            flags=cv2.cv.CV_HAAR_SCALE_IMAGE
        else:
            flags=cv2.CASCADE_SCALE_IMAGE

        faces = faceCascade.detectMultiScale(
            gray,
            scaleFactor=2,
            minNeighbors=1,
            minSize=(30, 30),
            flags = flags
        )

        for (x, y, w, h) in faces:
            face.Face = Classes.Rectangle(x, y, w, h)
            #delete next line after development
            cv2.rectangle(img, (x, y), (x + w, y + h), (255, 0, 0), 2)
            roi_gray = gray[y:y + h, x:x + w]
            roi_color = img[y:y + h, x:x + w]

            # ojo izquierdo
            ojoI = leyeCascade.detectMultiScale(roi_gray)
            for (x2, y2, w2, h2) in ojoI:
                face.LeftEye = Classes.Rectangle(x2, y2, w2, h2)
                #delete next line after development
                cv2.rectangle(roi_color, (x2, y2), (x2 + w2, y2 + h2), (0, 255, 0), 2)
                break
            # ojo derecho
            ojoD = reyeCascade.detectMultiScale(roi_gray)
            for (x2, y2, w2, h2) in ojoD:
                face.RightEye = Classes.Rectangle(x2, y2, w2, h2)
                #delete next line after development
                cv2.rectangle(roi_color, (x2, y2), (x2 + w2, y2 + h2), (0, 215, 0), 2)
                break
            # boca
            mouth = mouthCascade.detectMultiScale(roi_gray)
            for (x2, y2, w2, h2) in mouth:
                face.Mouth = Classes.Rectangle(x2, y2, w2, h2)
                #delete next line after development
                cv2.rectangle(roi_color, (x2, y2), (x2 + w2, y2 + h2), (0, 0, 255), 2)
                break
            # Nariz
            nose = noseCascade.detectMultiScale(roi_gray)
            for (x2, y2, w2, h2) in nose:
                face.Nose = Classes.Rectangle(x2, y2, w2, h2)
                #delete next line after development
                cv2.rectangle(roi_color, (x2, y2), (x2 + w2, y2 + h2), (0, 255, 255), 2)
                break
            break

        # Delete next 3 lines after development
        cv2.imshow('img', img)
        # close when q keyis pressed for a sec
        k = cv2.waitKey(30) & 0xff
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break;

    cap.release()
    return face
# Class which you send an image and you get the picture boxes according to haarcascades
def GetDataFromFile(direccion):
    # Start video capture
    face = Classes.RawFace()

    while not face.CheckValues():
        # hacer una captura
        img = cv2.imread(direccion, 1)
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        face = Classes.RawFace()

        if hasattr(cv2, "cv"):
            flags=cv2.cv.CV_HAAR_SCALE_IMAGE
        else:
            flags=cv2.CASCADE_SCALE_IMAGE

        faces = faceCascade.detectMultiScale(
            gray,
            scaleFactor=2,
            minNeighbors=1,
            minSize=(30, 30),
            flags = flags
        )

        for (x, y, w, h) in faces:
            face.Face = Classes.Rectangle(x, y, w, h)
            #delete next line after development
            cv2.rectangle(img, (x, y), (x + w, y + h), (255, 0, 0), 2)
            roi_gray = gray[y:y + h, x:x + w]
            roi_color = img[y:y + h, x:x + w]

            # ojo izquierdo
            ojoI = leyeCascade.detectMultiScale(roi_gray)
            for (x2, y2, w2, h2) in ojoI:
                face.LeftEye = Classes.Rectangle(x2, y2, w2, h2)
                #delete next line after development
                cv2.rectangle(roi_color, (x2, y2), (x2 + w2, y2 + h2), (0, 255, 0), 2)
                break
            # ojo derecho
            ojoD = reyeCascade.detectMultiScale(roi_gray)
            for (x2, y2, w2, h2) in ojoD:
                face.RightEye = Classes.Rectangle(x2, y2, w2, h2)
                #delete next line after development
                cv2.rectangle(roi_color, (x2, y2), (x2 + w2, y2 + h2), (0, 215, 0), 2)
                break
            # boca
            mouth = mouthCascade.detectMultiScale(roi_gray)
            for (x2, y2, w2, h2) in mouth:
                face.Mouth = Classes.Rectangle(x2, y2, w2, h2)
                #delete next line after development
                cv2.rectangle(roi_color, (x2, y2), (x2 + w2, y2 + h2), (0, 0, 255), 2)
                break
            # Nariz
            nose = noseCascade.detectMultiScale(roi_gray)
            for (x2, y2, w2, h2) in nose:
                face.Nose = Classes.Rectangle(x2, y2, w2, h2)
                #delete next line after development
                cv2.rectangle(roi_color, (x2, y2), (x2 + w2, y2 + h2), (0, 255, 255), 2)
                break
            break

        # Delete next 3 lines after development
        cv2.imshow('img', img)
        # close when q keyis pressed for a sec
        k = cv2.waitKey(30) & 0xff
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break;
    return face
