import os.path
import numpy as np

def train(cara, autism):
	# input dataset
	x = np.array([cara]).T


	autismo = []
	for z in cara:
		if  autism:
			autismo += [1.0]
		else:
			autismo += [0.0]
	y = np.array([autismo]).T

	# seed random numbers to make calculation
	# deterministic (just a good practice)
	np.random.seed(1)

	#revisar existencia
	syn0 = []
	if os.path.isfile("conocimiento.npy"):
		syn0 = np.load("conocimiento.npy")
	else:
		# initialize weights randomly with mean 0
		syn0 = (2*np.random.random((len(cara), 1)) - 1).T

	for iter in xrange(100):
		# forward propagation
		l0 = x
		l1 = nonlin(np.dot(l0,syn0))

		# how much did we miss?
		l1_error = y - l1

		# multiply how much we missed by the
		# slope of the sigmoid at the values in l1
		l1_delta = l1_error * nonlin(l1,True)
		# update weights
		syn1 = np.dot(l0.T,l1_delta)
		syn0 += syn1

	np.save("conocimiento", syn0)

def GetResults(cara):
	l1 = nonlin(np.dot(x,syn0))

	print l1
	
	# input dataset
	x = np.array([cara]).T
	# seed random numbers to make calculation
	# deterministic (just a good practice)
	np.random.seed(1)

	#revisar existencia
	syn0 = []
	if os.path.isfile("conocimiento.npy"):
		syn0 = np.load("conocimiento.npy")
	else:
		# initialize weights randomly with mean 0
		syn0 = (2*np.random.random((len(cara), 1)) - 1).T

	l1 = nonlin(np.dot(l0,syn0))

	r = 0
	for n in l1:
		r += sum(n)/len(n)
	r /= len(cara)
	print (str(r * 100) + "%")
	return r

# sigmoid function
def nonlin(x,deriv=False):
	if(deriv==True):
		return x*(1-x)
	return 1/(1+np.exp(-x))
