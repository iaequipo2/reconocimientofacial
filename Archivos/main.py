import sys
import GetPictureSquares
import Filter
import Distancias
import cv2
import nn
from PyQt4.QtCore import *
from PyQt4.QtGui import *
from Form import InputDialog

def afterSaveInfo():
    # Repeat indefinitely
    while 1:
        # obtener imagen de captura
        face = GetPictureSquares.GetData()

        # filtrar imagen
        check = Filter.CheckFace(face)

        # si se hacepto la imagen
        if check:
            # obtenr los datos de sus distancias
            faceDistances = Distancias.GetDistances(face)

            # calcular con numpy
            resultados = nn.GetResults(faceDistances)

        # close when q keyis pressed for a sec
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break;

    cv2.destroyAllWindows()

def main():
    app = QApplication(sys.argv)
    dialog = InputDialog()
    dialog.setAfterSave(afterSaveInfo)
    dialog.show()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
